
export const CONSTS = {
    ROOM_TYPE_DEF: 0, //默认
    ROOM_TYPE_MEETING: 1, //会议室

    GATE_ONLINE_STATUS_ON:1, //在线
    GATE_ONLINE_STATUS_OFF:0, //离线

    GATE_FACTORY_CODE_DAHUA:'dahua', //大华
    GATE_FACTORY_CODE_HIK: 'hikvision', //海康
    
    GATE_OPEN_TYPE_REMOTE: 5, //远程开门
    GATE_OPEN_TYPE_FACE: 16, //人脸认证
    GATE_OPEN_TYPE_PASSWORD: 22, //密码开门
}


//房间类型字典
export function getRoomTypes() {
    return [{value:CONSTS.ROOM_TYPE_DEF,label:'默认'},{value:CONSTS.ROOM_TYPE_MEETING,label:'会议室'}]
}

/*
 * @Description: 房间类型
 */
export function getRoomTypeLabel(type) {
    if(type==CONSTS.ROOM_TYPE_DEF){
        return '默认'
    }else if(type==CONSTS.ROOM_TYPE_MEETING){
        return '会议室'
    }
    return ''
}

//门禁在线状态
export function getGateOnlineStatusLabel(status) {
    if(status==CONSTS.GATE_ONLINE_STATUS_ON){
        return '在线'
    }else if(status==CONSTS.GATE_ONLINE_STATUS_OFF){
        return '离线'
    }
    return ''
}

//门禁品牌
export function getGateFactoryCodes() {
    return [{value:CONSTS.GATE_FACTORY_CODE_DAHUA,label:'大华'},{value:CONSTS.GATE_FACTORY_CODE_HIK,label:'海康'}]
}

//开门方式
export function getGateOpenTypeLabel(type) {
    if(type==CONSTS.GATE_OPEN_TYPE_REMOTE){
        return '远程开门'
    }else if(type==CONSTS.GATE_OPEN_TYPE_FACE){
        return '人脸识别'
    }else if(type==CONSTS.GATE_OPEN_TYPE_PASSWORD){
        return '密码开门'
    }
    return ''
}