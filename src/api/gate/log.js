
/*
 * @Description: 
 */
import request from '@/utils/request'

// 查询开门日志列表
export function listLog(data) {
  return request({
    url: '/gate/open/log/page-query',
    method: 'POST',
    data: data
  })
}

// 保存或添加房间
export function saveRoom(data) {
  return request({
    url: '/gate/manage/save',
    method: 'post',
    data: data
  })
}

// 查询房间详细
export function getRoom(id) {
  return request({
    url: '/gate/manage/get/' + id,
    method: 'get'
  })
}

// 查询房间详细
export function delRoom(id) {
  return request({
    url: '/gate/manage/delete/' + id,
    method: 'post'
  })
}