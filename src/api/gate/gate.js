
/*
 * @Description:
 */
import request from '@/utils/request'

// 查询房间门禁列表
export function listRoom(data) {
  return request({
    url: '/gate/manage/page-query',
    method: 'POST',
    data: data
  })
}

// 查询房间门禁列表(不包含某个用户)
export function listRoomNoUser(data) {
  return request({
    url: '/gate/manage/page-query-no-user',
    method: 'POST',
    data: data
  })
}

// 保存或添加房间
export function saveRoom(data) {
  return request({
    url: '/gate/manage/save',
    method: 'post',
    data: data
  })
}

// 查询房间详细
export function getRoom(id) {
  return request({
    url: '/gate/manage/get/' + id,
    method: 'get'
  })
}

// 删除房间
export function delRoom(id) {
  return request({
    url: '/gate/manage/delete/' + id,
    method: 'post'
  })
}
