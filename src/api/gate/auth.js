

/*
 * @Description: 认证人员管理
 */
import request from '@/utils/request'

// 查询房间详情-包含用户列表
export function getRoomDetail(roomId) {
  return request({
    url: '/gate/manage/get/'+roomId,
    method: 'GET'
  })
}

//房间新增人员
export function addGateUser(roomId, userIdList,remoteOpenList) {
  return request({
    url: '/gate/manage/add/user',
    method: 'POST',
    data: {
      roomId: roomId,
      userIdList: userIdList,
      deptIdList: [],
      remoteOpenList: remoteOpenList
    }
  })
}

//单个用户分配多个房间权限
export function addUserGates(userId, gateIdList, remoteOpenList) {
  return request({
    url: '/gate/manage/add/user-gates',
    method: 'POST',
    data: {
      userId: userId,
      roomIdList: gateIdList,
      remoteOpenList: remoteOpenList
    }
  })
}

// 查询房间详情-包含用户列表
export function delGateUser(roomId, userId) {
  return request({
    url: '/gate/manage/delete/user',
    method: 'POST',
    data: {
      roomId: roomId,
      wxUserId: userId
    }
  })
}

//修改房间用户远程权限
export function changeUserRemote(roomId, wxUserId,remoteOpen) {
  return request({
    url: '/gate/manage/user/remote',
    method: 'POST',
    data: {
      roomId: roomId,
      wxUserId: wxUserId,
      remoteOpen: remoteOpen
    }
  })
}

