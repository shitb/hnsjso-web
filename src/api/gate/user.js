/*
 * @Description: 
 */
import request from '@/utils/request'

// 查询所有员工
export function listUser(data) {
  return request({
    url: '/gate/user/page-query',
    method: 'POST',
    data: data
  })
}

//查询照片附件
export function getFile(bid) {
  return request({
    url: '/gate/user/pic/' + bid,
    method: 'post'
  })
}

//设置照片附件
export function setAvatar(id,bid) {
  return request({
    url: '/gate/user/pic/upload',
    method: 'post',
    data: {
      id: id,
      fileBid: bid
    }
  })
}

//查询基本信息
export function getUser(id) {
  return request({
    url: '/gate/user/get/info/' + id,
    method: 'GET'
  })
}

//更新基本信息
export function updateUser(form) {
  return request({
    url: '/gate/user/update',
    method: 'post',
    data: form
  })
}

//查询基本信息
export function delUser(id) {
  return request({
    url: '/gate/user/delete/' + id,
    method: 'POST'
  })
}

//新增基本信息
export function addUser(form) {
  return request({
    url: '/gate/user/add',
    method: 'post',
    data: form
  })
}

// 查询员工分配的房间
export function queryUserPermis(userId) {
  return request({
    url: '/gate/user/permis/'+userId,
    method: 'GET'
  })
}