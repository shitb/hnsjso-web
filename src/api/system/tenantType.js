import request from '@/utils/request'

// 创建租户类型
export function createTenantType(data) {
    return request({
        url: '/system/tenantType/create',
        method: 'post',
        data: data
    })
}

// 主键获取详情
export function getTenantType(id) {
    return request({
        url: '/system/tenantType/get/' + id,
        method: 'get'
    })
}

// 获取所有租户类型列表
export function getAllTenantType() {
    return request({
        url: '/system/tenantType/list-all',
        method: 'get'
    })
}

// 分页查询
export function getTenantTypePage(data) {
    return request({
        url: '/system/tenantType/page',
        method: 'post',
        data: data
    })
}

// 修改租户类型
export function updateTenantType(data) {
    return request({
        url: '/system/tenantType/update',
        method: 'post',
        data: data
    })
}

// 删除租户类型
export function delTenantType(id) {
    return request({
        url: '/system/tenantType/delete/' + id,
        method: 'get'
    })
}
