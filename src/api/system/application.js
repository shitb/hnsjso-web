import request from '@/utils/request'

// 创建应用
export function createApplication(data) {
    return request({
        url: '/system/application/create',
        method: 'post',
        data: data
    })
}

// 主键获取详情
export function getApplication(id) {
    return request({
        url: '/system/application/get/' + id,
        method: 'get'
    })
}

// 获取所有应用列表
export function getAllApplication() {
    return request({
        url: '/system/application/list-all',
        method: 'get'
    })
}

// 分页查询
export function getApplicationPage(data) {
    return request({
        url: '/system/application/page',
        method: 'post',
        data: data
    })
}

// 修改应用
export function updateApplication(data) {
    return request({
        url: '/system/application/update',
        method: 'post',
        data: data
    })
}

// 删除应用
export function delApplication(id) {
    return request({
        url: '/system/application/delete/' + id,
        method: 'get'
    })
}
