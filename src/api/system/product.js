import request from '@/utils/request'

// 创建产品
export function createProduct(data) {
    return request({
        url: '/system/product/create',
        method: 'post',
        data: data
    })
}

// 主键获取详情
export function getProduct(id) {
    return request({
        url: '/system/product/get/' + id,
        method: 'get'
    })
}

// 获取所有产品列表
export function getAllProduct() {
    return request({
        url: '/system/product/list-all',
        method: 'get'
    })
}

// 分页查询
export function getProductPage(query) {
    return request({
        url: '/system/product/page',
        method: 'get',
        params: query
    })
}

// 修改产品
export function updateProduct(data) {
    return request({
        url: '/system/product/update',
        method: 'post',
        data: data
    })
}

// 删除产品
export function delProduct(id) {
    return request({
        url: '/system/product/delete/' + id,
        method: 'get'
    })
}
