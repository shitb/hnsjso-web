import request from '@/utils/request'

// 分页查询
export function scheduleBpmPage(data) {
    return request({
        url: '/project/scheduleBpm/page-query',
        method: 'post',
        data: data
    })
}
// 项目进度填报
export function scheduleBpmSave(data) {
    return request({
        url: '/project/scheduleBpm/save',
        method: 'post',
        data: data
    })
}
// 项目进度填报
export function scheduleBpmProcSubmit(data) {
    return request({
        url: '/project/scheduleBpm/proc-submit',
        method: 'post',
        data: data
    })
}
// 主键查询进度填报详情
export function scheduleBpmGetInfo(id) {
    return request({
        url: `/project/scheduleBpm/get/${id}`,
        method: 'get',
    })
}