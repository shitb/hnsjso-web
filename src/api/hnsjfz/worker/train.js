import request from '@/utils/request'

// 新增
export function workerTrainCreate(data) {
    return request({
        url: '/worker/train/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function workerTrainPage(data) {
    return request({
        url: '/worker/train/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function workerTrainUpdate(data) {
    return request({
        url: '/worker/train/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function workerTrainDelList(ids) {
    return request({
        url: '/worker/train/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function workerTrainDel(id) {
    return request({
        url: `/worker/train/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function workerTrainGet(id) {
    return request({
        url: `/worker/train/get/${id}`,
        method: 'get'
    })
}
// 列表查询培训记录(共用)
export function workerTrainCommonGet(id) {
    return request({
        url: `/worker/train/list/${id}`,
        method: 'get'
    })
}