import request from '@/utils/request'

// 新增
export function workerbankcardCreate(data) {
    return request({
        url: '/worker/bankcard/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function workerbankcardPage(data) {
    return request({
        url: '/worker/bankcard/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function workerbankcardUpdate(data) {
    return request({
        url: '/worker/bankcard/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function workerbankcardDelList(ids) {
    return request({
        url: '/worker/bankcard/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function workerbankcardDel(id) {
    return request({
        url: `/worker/bankcard/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function workerbankcardGet(id) {
    return request({
        url: `/worker/bankcard/get/${id}`,
        method: 'get'
    })
}

// 列表查询银行卡(共用)
export function workerbankcardCommonGet(id) {
    return request({
        url: `/worker/bankcard/list/${id}`,
        method: 'get'
    })
}
