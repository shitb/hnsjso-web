import request from '@/utils/request'

// 获取字典数据
export function commonData() {
    return request({
        url: '/comm/dict/listAll',
        method: 'get'
    })
}
export function getSip() {
    return request({
        url: "/runde/info/get-sip",
        method: 'get'
    })
}
// 流程类型
export function procdefListAll() {
    return request({
        url: "/bpm/procdef/list-all",
        method: 'get'
    })
}
