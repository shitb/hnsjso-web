import request from '@/utils/request'

// 批量新增项目设备
export function deviceCreate(data) {
    return request({
        url: '/project/device/create',
        method: 'post',
        data: data
    })
}

// 分页查询项目设备
export function devicePage(data) {
    return request({
        url: '/project/device/page-device',
        method: 'post',
        data: data
    })
}

// 批量移除项目设备
export function deviceDelList(data) {
    return request({
        url: '/project/device/remove',
        method: 'post',
        data: data
    })
}

// 主键查询设备详情(查看)
export function deviceGet(id) {
    return request({
        url: `/project/device/getDeviceInfo/${id}`,
        method: 'get'
    })
}

// 项目设备绑定工人
export function deviceBindWorker(data) {
    return request({
        url: '/project/device/bind-worker',
        method: 'post',
        data: data
    })
}

// 主键查询工人详情(绑定)
export function getWorkerInfo(workerId) {
    return request({
        url: `/project/device/getWorkerInfo/${workerId}`,
        method: 'get',
    })
}

// 分页查询项目可选设备
export function deviceSelectBind(data) {
    return request({
        url: '/project/device/page-select-device',
        method: 'post',
        data: data
    })
}

// 分页查询项目可绑工人
export function deviceSelectWorker(data) {
    return request({
        url: '/project/device/page-select-worker',
        method: 'post',
        data: data
    })
}

// 项目设备解绑工人
export function deviceUnBindWorker(data) {
    return request({
        url: '/project/device/unbind-worker',
        method: 'post',
        data: data
    })
}