import request from '@/utils/request'

// 新增
export function scheduleCreate(data) {
    return request({
        url: '/project/schedule/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function schedulePage(data) {
    return request({
        url: '/project/schedule/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function scheduleUpdate(data) {
    return request({
        url: '/project/schedule/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function scheduleDelList(ids) {
    return request({
        url: '/project/schedule/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function scheduleDel(id) {
    return request({
        url: `/project/schedule/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function scheduleGet(id) {
    return request({
        url: `/project/schedule/get/${id}`,
        method: 'get'
    })
}

