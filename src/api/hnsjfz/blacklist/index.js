import request from '@/utils/request'


// 新增
export function blacklistCreate(data) {
    return request({
        url: '/worker/blacklist/create',
        method: 'post',
        data: data
    })
}
// 分页查询
export function blacklistPage(data) {
    return request({
        url: '/worker/blacklist/page-query',
        method: 'post',
        data: data
    })
}

// 批量删除
export function blacklistDelList(ids, reason) {
    return request({
        url: '/worker/blacklist/remove',
        method: 'post',
        data: {
            idList: ids,
            reason: reason
        }
    })
}

// 主键查询详情
export function blacklistGet(id) {
    return request({
        url: `/worker/blacklist/get/${id}`,
        method: 'get'
    })
}

// 列表查询黑名单变更记录
export function blacklistChglog(id) {
    return request({
        url: `/worker/blacklist/list-chglog/${id}`,
        method: 'get'
    })
}
