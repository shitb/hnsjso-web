import request from '@/utils/request'

// 新增
export function laborCompanyCreate(data) {
    return request({
        url: '/cfg/laborCompany/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function laborCompanyPage(data) {
    return request({
        url: '/cfg/laborCompany/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function laborCompanyUpdate(data) {
    return request({
        url: '/cfg/laborCompany/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function laborCompanyDelList(ids) {
    return request({
        url: '/cfg/laborCompany/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function laborCompanyDel(id) {
    return request({
        url: `/cfg/laborCompany/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function laborCompanyGet(id) {
    return request({
        url: `/cfg/laborCompany/get/${id}`,
        method: 'get'
    })
}

// 查询所有
export function laborCompanyGetAllList() {
    return request({
        url: `/cfg/laborCompany/list-all`,
        method: 'get'
    })
}
