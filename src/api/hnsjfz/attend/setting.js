import request from '@/utils/request'

// 新增
export function attendSettingCreate(data) {
    return request({
        url: '/attend/setting/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function attendSettingPage(data) {
    return request({
        url: '/attend/setting/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function attendSettingUpdate(data) {
    return request({
        url: '/attend/setting/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function attendSettingDelList(ids) {
    return request({
        url: '/attend/setting/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function attendSettingDel(id) {
    return request({
        url: `/attend/setting/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function attendSettingGet(id) {
    return request({
        url: `/attend/setting/get/${id}`,
        method: 'get'
    })
}

// 分页查询我的项目
export function getMyProjectPage(data) {
    return request({
        url: '/attend/setting/page-myproject',
        method: 'post',
        data: data
    })
}

// 查询项目所有围栏
export function getProjectFence(projectId) {
    return request({
        url: `/attend/setting/list-fence-all/${projectId}`,
        method: 'get'
    })
}