import request from '@/utils/request'

// 新增
export function attendPieceCreate(data) {
    return request({
        url: '/attend/piece/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function attendPiecePage(data) {
    return request({
        url: '/attend/piece/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function attendPieceUpdate(data) {
    return request({
        url: '/attend/piece/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function attendPieceDelList(ids) {
    return request({
        url: '/attend/piece/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function attendPieceDel(id) {
    return request({
        url: `/attend/piece/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function attendPieceGet(id) {
    return request({
        url: `/attend/piece/get/${id}`,
        method: 'get'
    })
}