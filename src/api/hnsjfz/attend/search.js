import request from '@/utils/request'

// 按项目分页查询
export function monthReportPage(data) {
    return request({
        url: '/attend/monthReport/page-project',
        method: 'post',
        data: data
    })
}

// 按项目导出
export function exportExcelProject(data) {
    return request({
        url: `/attend/monthReport/excel-project`,
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}

// 按工人分页查询
export function monthReportWorkerPage(data) {
    return request({
        url: '/attend/monthReport/page-worker',
        method: 'post',
        data: data
    })
}

// 按工人导出
export function exportExcelWorker(data) {
    return request({
        url: `/attend/monthReport/excel-worker`,
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}

// 按工人分页查询
export function monthReportInfoPage(data) {
    return request({
        url: '/attend/monthReport/page-detail',
        method: 'post',
        data: data
    })
}

// 按工人导出
export function exportExcelDetail(data) {
    return request({
        url: `/attend/monthReport/excel-detail`,
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}