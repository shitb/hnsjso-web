import request from '@/utils/request'

// 分页查询
export function attendRecordPage(data) {
    return request({
        url: '/attend/record/page-sumday',
        method: 'post',
        data: data
    })
}
// 分页查询考勤详情
export function attendRecordDetail(data) {
    return request({
        url: '/attend/record/page-detail',
        method: 'post',
        data: data
    })
}
