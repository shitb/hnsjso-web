import request from '@/utils/request'

// 分页查询工人信息
export function trackWorkerList(data) {
    return request({
        url: '/attend/track/page-worker',
        method: 'post',
        data: data
    })
}

// 查询轨迹列表
export function trackList(data) {
    return request({
        url: '/attend/track/list-track',
        method: 'post',
        data: data
    })
}
