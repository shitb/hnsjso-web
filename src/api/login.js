import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid) {
  const data = {
    username,
    password,
    code,
    uuid
  }
  return request({
    url: '/system/comm/login',
    method: 'post',
    data: data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/system/permission/get-permission-info',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/system/comm/logout',
    method: 'get'
  })
}

// 获取验证码
export function getCodeImg(username) {
  return request({
    url: '/system/comm/captcha/get-image?username=' + username,
    method: 'get'
  })
}
