/*
 * @Author: 419754735@qq.com shitb13938457325
 * @Date: 2024-01-17 19:26:09
 * @LastEditors: 419754735@qq.com shitb13938457325
 * @LastEditTime: 2024-02-29 17:47:49
 * @FilePath: /manage_demo/src/router/index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import ParentView from '@/components/ParentView';

/**
 * Note: 路由配置项
 *
 * hidden: true                   // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true               // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect           // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'             // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * meta : {
    noCache: true                // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'               // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'             // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false            // 如果设置为false，则不会在breadcrumb面包屑中显示
  }
 */

// 公共路由
export const constantRoutes = [
  {
    path: '/login',
    component: (resolve) => require(['@/views/login'], resolve),
    hidden: true
  },
  {
    path: '/404',
    component: (resolve) => require(['@/views/error/404'], resolve),
    hidden: true
  },
  {
    path: '/401',
    component: (resolve) => require(['@/views/error/401'], resolve),
    hidden: true
  },
  // {
  //   path: '',
  //   component: Layout,
  //   redirect: 'index',
  //   children: [
  //     {
  //       path: 'index',
  //       component: (resolve) => require(['@/views/index'], resolve),
  //       name: '工作台',
  //       meta: { title: '工作台', icon: 'dashboard', noCache: true, affix: true }
  //     },
  //   ]
  // },
  {
    path: '/',
    component: Layout,
    redirect: 'home',
    name: '门禁管理',
    meta: { title: '门禁管理' , icon:'password'},
    children: [
      {
        path: 'home',
        component: (resolve) => require(['@/views/gate/index'], resolve),
        name: 'home',
        meta: { title: '门禁管理', icon:'validCode' }
      },
      {
        path: 'user',
        component: (resolve) => require(['@/views/gate/user'], resolve),
        name: 'user',
        meta: { title: '人员查看', icon:'checkbox' }
      },
      {
        path: 'log',
        component: (resolve) => require(['@/views/gate/log'], resolve),
        name: 'log',
        meta: { title: '门禁日志', icon:'druid' }
      },
      {
        path: 'auth/:roomId',
        hidden: true,
        component: (resolve) => require(['@/views/gate/auth'], resolve),
        name: 'auth',
        meta: { title: '认证人员', icon:'password' }
      },
      {
        path: 'permis/:userId',
        hidden: true,
        component: (resolve) => require(['@/views/gate/permis'], resolve),
        name: 'permis',
        meta: { title: '人员权限', icon:'password' }
      }
    ]
  },
]

export default new Router({
  mode: 'history', // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
